mccs (1:1.1-10) unstable; urgency=medium

  [ Adrian Bunk ]
  * Build with -std=gnu++14 to workaround FTBFS with gcc >= 11.
    (Closes: #984229)
  * Build with -g to add debug symbols to mccs-dbgsym.

  [ Ralf Treinen ]
  * Standards-Version 4.6.2 (no change) 
  * Declare Root-Requires-Root=no

  [ Debian Janitor ]
  * Trim trailing whitespace.

 -- Ralf Treinen <treinen@debian.org>  Mon, 16 Jan 2023 20:57:37 +0100

mccs (1:1.1-9) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_ARTIFACTS instead of ADT_ARTIFACTS
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces

  [ Ralf Treinen ]
  * d/control: typo in short description
  * Apply patch by Helmut Grohne (thanks) to use a standard variable for
    compiler flags (closes: #961696)
  * Build-depend on debhelper-compat, drop file debian/compat.
  * Debhelper compatibility level 13 (no change)
  * Standards-Version 4.5.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Thu, 28 May 2020 11:52:06 +0200

mccs (1:1.1-8) unstable; urgency=medium

  * Updated Vcs-* to salsa
  * Standards-Version 4.2.0 (no change)
  * Removed Homepage and debian/watch since upstream URL does no longer
    work.

 -- Ralf Treinen <treinen@debian.org>  Tue, 14 Aug 2018 20:32:33 +0200

mccs (1:1.1-7) unstable; urgency=medium

  * patch no-lfl: do not link with libfl. Thanks a lot to Adrian Bunk for
    the patch! (closes: #891521). Drop libfl-dev from build-dependencies.
  * Standards-Version 4.1.3:
    - priority optional
    - d/copyright: use https: in format specifier
  * DH compat level 11, bump build dependency on debhelper.

 -- Ralf Treinen <treinen@debian.org>  Wed, 07 Mar 2018 20:54:08 +0100

mccs (1:1.1-6) unstable; urgency=medium

  * package test legacy: add Dependency on @

 -- Ralf Treinen <treinen@debian.org>  Sun, 19 Feb 2017 11:58:39 +0100

mccs (1:1.1-5) unstable; urgency=medium

  * debian/rules: build with --no-parallel to fix parallel build failures.
    Thanks a lot to Adrian Bunk for the patch! (closes: #847761).
  * Capitalization error in long description.
  * Vcs-{Browser,Git}: use secure URI

 -- Ralf Treinen <treinen@debian.org>  Sun, 05 Feb 2017 10:24:10 +0100

mccs (1:1.1-4) unstable; urgency=medium

  * Add build-dependency on libfl-dev (closes: #846441). Thanks to
    Helmut Grohne for the heads-up.
  * Standards-Version 3.9.8 (no change)
  * Debhelper compatibility level 10:
    - bump value in debian/compat
    - bump versioned build-dependency on debhelper.
  * debian/tests/legacy: create files in ${ADT_ARTIFACTS} instead of $TMPDIR

 -- Ralf Treinen <treinen@debian.org>  Thu, 01 Dec 2016 22:49:28 +0100

mccs (1:1.1-3) unstable; urgency=low

  * Migrate packaging to git:
    - drop debian/svn-deblayout, create debian/gbp.conf
    - update Vcs-* fields in debian/control.
    - debian/rules: create an empty objs directory since git does not
      track empty directories.
  * debhelper compat level 9; bump version in build-dependency on debhelper
  * Add patch hardening: pass $(LDFLAGS) to the compiler
  * Standards-Version 3.9.5 (no change)
  * Drop build-dependency on coinor solver which is unnecessary, replace
    binary dependency by coinor-cbc to prepare for the upcoming coinor
    transition. Thanks to Miles Lubin for the patch! (closes: #730323).
  * Add as-installed package test:
    - add DEP-8 style debian/tests/control
    - add debian/tests/legacy test script

 -- Ralf Treinen <treinen@debian.org>  Tue, 26 Nov 2013 08:48:00 +0100

mccs (1:1.1-2) unstable; urgency=low

  * /usr/share/cudf/solvers/mccs-*: drop -lex[] wrapper from the optimisation
    criterion, in order to allow to use the full power of mccs.
  * standards-version 3.9.3:
    - complete migration to machine readable copyright format 1.0:
      add format line, fix spelling

 -- Ralf Treinen <treinen@debian.org>  Wed, 23 May 2012 22:01:45 +0200

mccs (1:1.1-1) unstable; urgency=low

  * New upstream version.
    - updated patches calling-engines and cbc-default
    - epoch necessary due to the "01" madness in previous upstream version

 -- Ralf Treinen <treinen@debian.org>  Thu, 25 Aug 2011 16:16:05 +0200

mccs (1.01.1-3) unstable; urgency=low

  * Drop wrapper scripts, use solver specifications with variable
    interpolation.

 -- Ralf Treinen <treinen@debian.org>  Fri, 10 Jun 2011 22:02:24 +0200

mccs (1.01.1-2) unstable; urgency=low

  * Wrapper scripts: add -lex[..] around the third argument, as required
    by mccs.

 -- Ralf Treinen <treinen@debian.org>  Mon, 06 Jun 2011 16:38:21 +0200

mccs (1.01.1-1) unstable; urgency=low

  * New upstream release (unfortunately, this release is still called
    1.01 by upstream).

 -- Ralf Treinen <treinen@debian.org>  Thu, 26 May 2011 02:28:15 +0200

mccs (1.01-1) unstable; urgency=low

  * New upstream release. This release should solve a bug which leads to
    wrong solutions in conjunction with the cbc engine.
  * Simplify debian/copyright: objs/cudf.tab.{c,h} are generated and not
    part of the source package. Thanks to Luca Falavigna for the hint.
  * Fix mistyped path name in man page (closes: #627548).

 -- Ralf Treinen <treinen@debian.org>  Tue, 24 May 2011 15:26:40 +0200

mccs (1.0-1) unstable; urgency=low

  * Initial package (closes: #625636).

 -- Ralf Treinen <treinen@debian.org>  Wed, 11 May 2011 19:47:48 +0200
